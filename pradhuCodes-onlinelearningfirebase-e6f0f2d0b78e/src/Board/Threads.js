import React, { Component } from 'react'
import { Spinner } from 'elemental'
import { styles } from 'refire-app'

import Thread from './Thread'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import { Link } from 'refire-app'
import LockIcon from 'react-icons/lib/fa/lock'

import receipeimg1 from '../static/paneer_butter1.jpg';
import receipeimg2 from '../static/chole.png';   
import receipeimg3 from '../static/sandwich.png';
import receipeimg4 from '../static/sushi.png'; 
import TextField from 'material-ui/TextField';

class Threads extends Component {

  handleChange = (event) => {
        this.setState({
          value: event.target.value,
        });
      };
 constructor(props) {
        super(props);

        this.state = {
          value: '',
        };
      }

  render() {
    const {
      boardId,
      threads,
      loaded, 
      styles, 
      theme,
    } = this.props
  if (!threads.length) {
    if (!loaded) {
      return (
        <div className={styles.spinnerContainer}>
          <Spinner />
        </div>
      )
    } else {
      return (
        <div className={styles.noThreadsYet}>
          No threads here yet
        </div>
      )
    }
  } else {
    let threads_new = {}
    threads_new = threads.filter((thread)=>thread.value!==null)
    threads_new = threads_new.filter((thread)=>thread.value.title.toLowerCase().includes(this.state.value.toLowerCase()))
    console.log(threads)
    return (
    <MuiThemeProvider>
        <div className={styles.root}>
          <TextField
            hintText="Search Your Favourite Recipe"
            onChange={this.handleChange}
            style = {{width: 1000, height: 50}} 
          /><br />
          <GridList
            styles={styles.gridList}
          >
            {
              threads_new.map(({ key, value: thread }) => {
                  let image = receipeimg1
                  if(thread.title==="Paneer Butter Masala") {
                    image = receipeimg1
                  } else if(thread.title==="Chole Bhature") {
                    image = receipeimg2
                  } else if(thread.title==="Sandwich") {
                    image = receipeimg3
                  } else if(thread.title==="Sushi") {
                    image = receipeimg4
                  }
                  let count = "Comments: " + Object.keys(thread.posts).length
                  let ingredients = "Ingredients: " + Object.keys(thread.Ingredient).length
                  return (
                  <Link to={`/board/${boardId}/${key}`} className={styles.title}>
                    <GridTile
                      key={key}
                      title={thread.title}
                      subtitle={count +"       "+ ingredients}
                      >
                      <img src={image} style = {{width: 300, height: 200}}  />
                    </GridTile>
                  </Link>
                  )
              })
            }
          </GridList>
      </div>
    </MuiThemeProvider>
    )
  }
}
}
const css = {
  spinnerContainer: {
    padding: "30px 0",
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    display: 'flex',
    flexWrap: 'nowrap',
    overflowX: 'auto',
  },
  search: {
    borderRadius: "20px",
    height: "50px",
    width: "200px",
    margin: "0 10px 0 0",
  },
  noThreadsYet: {},
}

export default styles(css, Threads)
