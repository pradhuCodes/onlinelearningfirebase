import React from 'react'
import { Link, styles } from 'refire-app'
import LockIcon from 'react-icons/lib/fa/lock'
import CommentsIcon from 'react-icons/lib/fa/comments'
import { fromNow } from '../utils'
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import receipeimg1 from '../static/paneer_butter1.jpg';
import receipeimg2 from '../static/chole.png';   
import receipeimg3 from '../static/sandwich.png';
import receipeimg4 from '../static/sushi.png'; 


const Thread = ({ threadKey, thread, boardId, styles }) => {
  if (!thread) return <div />
  const locked = thread.locked
    ? <LockIcon />
    : <span />
  let image = receipeimg1
    if(thread.title==="Paneer Butter Masala") {
      image = receipeimg1
    } else if(thread.title==="Chole Bhature") {
      image = receipeimg2
    } else if(thread.title==="Sandwich") {
      image = receipeimg3
    } else if(thread.title==="Sushi") {
      image = receipeimg4
    }
  return (
    <div className={styles.threadContainer} key={threadKey}>
      <Link to={`/board/${boardId}/${threadKey}`} className={styles.title}>
        <GridTile
          key={image}
          title={thread.title}
          actionIcon={<IconButton><StarBorder color="rgb(0, 188, 212)" /></IconButton>}
          titleStyle={styles.titleStyle}
          titleBackground="linear-gradient(to top, rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.3) 70%,rgba(0,0,0,0) 100%)"
        >
          <img src={image} />
        </GridTile>
      </Link>
      <div className={styles.metaContainer}>
        <div className={styles.lockedContainer}>
          {locked}
        </div>
        <div className={styles.profileContainer}>
          <Link to={`/profile/${thread.user.id}`} title={thread.user.displayName}>
            <img src={thread.user.image} className={styles.image} />
          </Link>
        </div>
        <Link to={`/board/${boardId}/${threadKey}`} className={styles.commentsContainer}>
          <span className={styles.commentsCount}>
            {Object.keys(thread.posts).length - 1}
          </span>
          <CommentsIcon />
        </Link>
        <div className={styles.lastPost}>
          {fromNow(thread.lastPostAt)}
        </div>
      </div>
    </div>
  )
}

const css = {
  threadContainer: {
    position: "relative",
    padding: "15px 0",
    borderBottom: "1px solid rgba(0, 0, 0, 0.1)",
    "&:last-child": {
      borderBottom: 0,
    },
  },
  title: {
    margin: "0",
    paddingRight: "100px",
    fontWeight: 500,
    display: "block",
  },
  image: {
    display: "none",
    "@media (min-width: 480px)": {
      display: "inline-block",
      width: "20px",
      height: "20px",
      borderRadius: "10px",
    },
  },
  lockedContainer: {
    display: "inline-block",
    verticalAlign: "middle",
    margin: "0 5px 0 0",
  },
  profileContainer: {
    display: "inline-block",
    position: "relative",
    verticalAlign: "middle",
  },
  metaContainer: {
    position: "absolute",
    right: 0,
    top: 0,
    padding: "13px 0",
  },
  commentsContainer: {
    display: "inline-block",
    minWidth: "40px",
    textAlign: "right",
    verticalAlign: "middle",
  },
  commentsCount: {
    display: "inline-block",
    margin: "0 5px 0 0",
  },
  lastPost: {
    display: "inline-block",
    minWidth: "70px",
    textAlign: "center",
    verticalAlign: "middle",
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: 550,
    height: 450,
    overflowY: 'auto',
  },
}

export default styles(css, Thread)
