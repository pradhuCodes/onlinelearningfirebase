import React, { Component } from 'react'
import EditPost from './EditPost'
import { FirebaseWrite, styles } from 'refire-app'
import { quote } from '../utils'
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import receipeimg from '../static/paneer_butter1.jpg';
import healthyimg from '../static/ic_favorite.png';   
import chefimg from '../static/gordon_ramsay.jpg'; 
import {List, ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';
import ActionFav from 'material-ui/svg-icons/action/favorite';
import ActionDiet from 'material-ui/svg-icons/maps/local-hospital';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import IconMenu from 'material-ui/IconMenu';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import ContentFilter from 'material-ui/svg-icons/content/filter-list';
import FileFileDownload from 'material-ui/svg-icons/file/file-download';

class Recipe extends Component {
  constructor(props) {
    super(props);

    this.state = {
    open: false,
  };
  }
  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };
  render() {
    const {
      user,
      threadKey,
      posts,
      Ingredient,
      postKey,
      quote,
      locked,
      selectLastPage,
      styles,
      theme,
    } = this.props
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.handleClose}
      />,
    ];
    let mapHealthy = new Map();
    let mapDietry = new Map();
    let newIngArr = [];
    let tips = [];
    let tipsBody = [];
    let printTips = ""
    if(Ingredient!==undefined) {
       Ingredient.forEach((ing) => {
        newIngArr.push(ing);
      });
    }

    if(posts!==undefined) {
        posts.forEach((post) => {
        if(post.value.ingredient!==undefined) {
          if(post.value.ingredient==="0" && post.value.category==="2") {
            newIngArr.push(post.value.body);
          } else {
            if(post.value.ingredientType==="Dietry") {
              if(mapDietry.get(post.value.ingredient)!==undefined) {
                  mapDietry.set(post.value.ingredient,mapDietry.get(post.value.ingredient)+", "+post.value.body);
                } else {
                  mapDietry.set(post.value.ingredient,post.value.body);
                }
              } else {
                if(mapHealthy.get(post.value.ingredient)!==undefined) {
                  mapHealthy.set(post.value.ingredient,mapHealthy.get(post.value.ingredient)+", "+post.value.body);
                } else {
                  mapHealthy.set(post.value.ingredient,post.value.body);
                }
              }
          }
        }
        console.log(post.value.category)
        if(post.value.category==="4") {
          var matches = post.value.body.match(/\bhttps?:\/\/\S+/gi);
          tips.push(matches[0]);
          tipsBody.push(post.value.body);
          printTips = printTips + " TIP --->"+matches[0]+"\n"
        }
      });
    }
    
    return (
    <div>
      <Card>
          <CardHeader title="Gordon Ramsay" subtitle="Vegan Specialist" avatar={chefimg}/>
          <CardMedia overlay={<CardTitle title="Paneer Butter Masala" subtitle="Perfect restaurant style paneer butter masala for all you paneer (cottage cheese) lovers." />}>
            <img style={{height:"550px"}}  src={receipeimg} alt="" />
          </CardMedia>
          <CardText>
          <br/>
        <RaisedButton label="Knowledge Tips" onClick={this.handleOpen} />
        <Dialog
          title="Explore Knowledge Tips"
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
        >
          {tips.map(function(tipItem,index) {
            return <a href={tipItem}><RaisedButton label={tipsBody[index]}/></a>;
          })}
        </Dialog>
        <ul/>
          <h3>Ingredients</h3>
          <ul>
            {newIngArr.map(function(ingredient,index) {
                index+=1;
                if(mapHealthy.get(index+"")!==undefined) {
                  return (<li>{ingredient}
                  <IconButton
                    iconStyle={{width: 13,height: 13,}}
                    style={{width: 5,height: 5,}}
                    tooltip={mapHealthy.get(index+"")}
                  ><ActionFav /></IconButton>
                  {mapDietry.get(index+"")!==undefined ? 
                  <IconButton
                    iconStyle={{width: 13,height: 13,}}
                    style={{width: 5,height: 5,marginLeft: 3}}
                    
                    tooltip={mapDietry.get(index+"")}
                  ><ActionDiet /></IconButton>: null}
                  </li>
                  
                 )
                } else if(mapDietry.get(index+"")!==undefined) {
                  return (<li>{ingredient}
                  <IconButton
                    iconStyle={{width: 13,height: 13,}}
                    style={{width: 5,height: 5,}}
                    tooltip={mapDietry.get(index+"")}
                  ><ActionDiet /></IconButton></li>
                 )
                } else {
                  return (<li>{ingredient}</li>)
                }
          },this)}
          </ul>
          <br/>
          <h3> Instructions </h3>
          <ol>
            <li>Heat 1 tablespoon butter in a pan. Add Cumin Seeds and fry them on low heat till they start to splutter.</li>
            <li>Add Green Cardamom (Choti Elaichi), Cinnamon (Dalchini) and Cloves (Laung) to butter. Fry on low flame them for a minute.</li>
            <li>Add Chopped Ginger and Garlic Cloves to the pan and cook for a minute. </li>
            <li>Add chopped onions and fry them till they turn translucent. There is no need to brown the onions.</li>
            <li>Add chopped tomatoes and cook them till they become soft.</li>
            <li>Add cashews to the mixture. If you want you can roast the cashews separately before adding them.</li>
            <li>Add 1 teaspoon Salt along with 1 Cup of water.</li>
            <li>Cover with a lid and cook for 15-20 minutes till the mixture becomes soft.</li>
            <li>Transfer the mixture to a blender or food processor and make a smooth fine paste. You can pass the paste through a sieve to remove the skin of the tomatoes and any other chunks.</li>
            <li>Again heat 1 tablespoon Butter in a pan and add a Bay Leaf (Tej Patta) and Red Chilli Powder to it. Fry it for a few seconds. This will give a nice color to the gravy.</li>
            <li>Add the onion tomato paste to the pan and mix it with the red chilli powder. Cook for a couple of minutes.</li>
            <li>Add Garam Masala, Coriander Powder (Dhania Powder) and Turmeric Powder (Haldi) to the mixture.</li>
            <li>Also add some Sugar to the gravy. Adding Sugar is optional but it gives a nice taste to the gravy. You can also replace Sugar with honey.</li>
            <li>Now add cream to the gravy and mix everything well. The cream will give a richness to the gravy. If you want to skip it you can use a little bit of milk instead.</li>
            <li>Sprinkle some crushed Dry Fenugreek Leaves (Kasuri Methi). Finally add the Paneer Cubes and cook everything for 3-4 minutes. If you want you can fry the Paneer cubes very slightly before adding them.</li>
            <li>Paneer Butter Masala is ready. You can garnish it with chopped Coriander, shredded Paneer or a drizzle of Cream. Serve it hot with any Indian Bread like Naan, Missi Roti, Lachha Parantha or with some rice preparation like Jeera Rice.</li>
          </ol>
          </CardText>
      </Card>
      </div>
    )
  }
}

const css = {
  container: {},
  displayName: {},
  userProfile: {
    margin: "0 0 10px 0",
  },
  profileImage: {
    borderRadius: "20px",
    height: "40px",
    width: "40px",
    margin: "0 10px 0 0",
  },
  plusIcon: {
    marginRight: "10px",
  },
  button: {
    cursor: "pointer",
    color: "#444",
    display: "inline-block",
    verticalAlign: "top",
    paddingRight: "20px",
  },
}

export default styles(
  css,
  FirebaseWrite({ method: "update" })(Recipe)
)
