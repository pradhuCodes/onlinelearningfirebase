import React, { Component } from 'react'
import { Button, Form, FormSelect } from 'elemental'
import { FirebaseWrite, styles } from 'refire-app'
import PlusIcon from 'react-icons/lib/fa/plus'
import { replaceEmojis, quote } from '../utils'
import { replyToThread, savePost } from '../updates'
import PreviewButton from '../App/PreviewButton'
import PreviewFields from './PreviewFields'
import TextFields from './TextFields'
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import ActionFavorite from 'material-ui/svg-icons/action/favorite';
import ActionFavorite2 from 'material-ui/svg-icons/maps/local-hospital';

class EditPost extends Component {

  constructor(props, context) {
    super(props, context)
    this.state = {
      text: props.text,
      previewEnabled: false,
      initialText: props.text,
      category:"1",
      hashtags:"",
      ingredient:"",
      IngredientData:props.IngredientData,
      ingredientType:"Dietry",
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.quote !== this.props.quote) {
      this.setState({
        text: quote(replaceEmojis(`${nextProps.quote}`)),
      }, () => {
        if (this.textInput) {
          this.textInput.scrollTop = this.textInput.scrollHeight
          this.textInput.focus()
        }
      })
    }
    if (nextProps.text !== this.props.text) {
      this.setState({
        text: nextProps.text,
        initialText: nextProps.text,
        hashtags: this.filter(nextProps.text)
      })
    }
    if(nextProps.category !== this.props.category) {
      this.setState({
        category:nextProps.category,
      })
    }
  }

  submit = (event) => {
    event.preventDefault()
    const { user, threadKey, selectLastPage, submit, replyToKey, editing, postKey, setShowEdit, category, hashtags,ingredient,ingredientData,ingredientType } = this.props
    if (editing) {
      const update = savePost({
        postKey: postKey,
        text: this.state.text,
        user: user,
      })
      submit(update)
      setShowEdit(false)
    } else {
      const update = replyToThread({
        threadKey,
        text: this.state.text,
        replyToKey,
        user,
        category:this.state.category,
        hashtags:this.filter(this.state.text),
        ingredient: this.state.category==="2" && this.state.ingredient==="" ? "0" : this.state.ingredient,
        ingredientType: this.state.ingredientType,
      })
      submit(update)
      selectLastPage()
      this.setState({ text: "" })
    }
  }

  filter = (text) => {
    var regexp = /\B\#\w\w+\b/g
    var result = text.match(regexp);
    if (result) {
        return result;
    }
    return "";
  }
  cancel = (event) => {
    event.preventDefault()
    this.props.setShowEdit(false)
    this.setState({ text: this.state.initialText })
  }

  updateField = (field) => {
    return (event) => {
      event.preventDefault()
      this.setState({ [field]: replaceEmojis(event.target.value) })
    }
  }

  togglePreview = () => {
    this.setState({ previewEnabled: !this.state.previewEnabled })
  }

  textInputRef = (input) => {
    this.textInput = input
    if (this.textInput && this.props.showEdit && this.props.cancelable) {
      this.textInput.focus()
    }
  }

  handleSelect = (input) => {
    this.setState({ category: input });
  }

  handleIngredientSelect = (input) => {
    this.setState({ ingredient: input });
  }
  handleIngredientType = (input1,input) => {
    this.setState({ ingredientType: input });
  }

  render() {
    const { user, locked, theme, buttonCaption, showEdit, cancelable, styles,IngredientData } = this.props
    const submitEnabled = !!this.state.text
    let output = []
    if(IngredientData!==undefined) {

      for(var i=1;i<this.props.IngredientData.length;i++) {
        var obj = {}
        obj["label"]=this.state.IngredientData[i]
        obj["value"]=i;
        output.push(obj)
      }
    }
    
    const cancelButton = cancelable
      ? (
        <Button
          onClick={this.cancel}
          hidden={true}
          className={styles.cancelButton}
        >
          Cancel
        </Button>
      )
      : null
    const saveIcon = cancelable
      ? null
      : <PlusIcon className={styles.plusIcon} />
    if (!user || locked || !showEdit) return <div />
    return (
      <div className='editPostContainer'>
        <Form>
          <FormSelect options={[
            { label: 'General',    value: "1" },
            { label: 'Ingredients',  value: "2" },
            { label: 'Instructions', value: "3" },
            { label: 'Knowledge', value: "4" },
            ]} firstOption="Choose Category" onChange={this.handleSelect} />

          {this.state.category==="2" ? (<FormSelect options={output} firstOption="Choose/Add Ingredient" onChange={this.handleIngredientSelect} />) : null}
          {this.state.category==="2" ? (
          <RadioButtonGroup onChange={this.handleIngredientType}name="IngredientType" defaultSelected="Dietry" style={{display: 'flex', flexDirection: 'row', margin:"10"}}>
            <RadioButton
              value="Healthy"
              label="Healthy"
              checkedIcon={<ActionFavorite style={{color: '#F44336'}} />}
              uncheckedIcon={<ActionFavorite />}
              style={styles.radioButton}
            />
            <RadioButton
              value="Dietry"
              label="Dietry"
              checkedIcon={<ActionFavorite2 style={{color: '#F44336'}} />}
              uncheckedIcon={<ActionFavorite2 />}
              style={styles.radioButton}
            />
          </RadioButtonGroup>
          ) : null}

          <TextFields
            preview={this.state.previewEnabled}
            text={this.state.text}
            updateText={this.updateField("text")}
            inputRef={this.textInputRef}
            styles={theme.TextFields}
          />
          <PreviewFields
            preview={this.state.previewEnabled}
            text={this.state.text}
            styles={theme.PreviewFields}
          />
          <Button
            disabled={!submitEnabled}
            type="success"
            onClick={this.submit}
          >
            {saveIcon} {buttonCaption}
          </Button>
          {cancelButton}
          <PreviewButton
            enabled={this.state.previewEnabled}
            togglePreview={this.togglePreview}
          />
        </Form>
      </div>
    )
  }
}

const css = {
  container: {},
  displayName: {},
  userProfile: {
    margin: "0 0 10px 0",
  },
  plusIcon: {
    marginRight: "10px",
  },
  cancelButton: {
    marginLeft: "5px",
  },
  radioButton: {
    marginBottom: 30,
    display: 'inline-block',
    width: '20px',
  },
}

export default styles(
  css,
  FirebaseWrite({ method: "update" })(EditPost)
)
