import React from 'react'
import { styles } from 'refire-app'
import ReactMarkdown from 'react-markdown'
import CodeBlock from '../App/CodeBlock'

const PostBody = ({
      post,
      hide,
      styles,
      thread,
    }) => {

  if (hide || !post) {
    return ( <div></div> )
  } else {
    let ingredient = "";
    if(thread!==undefined && post!==undefined && post.ingredient!==undefined) {
        ingredient = thread.Ingredient[post.ingredient]
    }
    if(ingredient===undefined) {
      ingredient = ""+post.body;
    } else if(post!==undefined) {
      ingredient += " ---> "+post.body+" ("+post.ingredientType+" Alternative) "
    }

    return (
      <div
        className={styles.bodyContainer}>
        <ReactMarkdown
          className={styles.markdown}
          escapeHtml={true}
          source={ingredient}
          renderers={
            {
              ...ReactMarkdown.renderers,
              ...{ CodeBlock },
            }
          }
        />
      </div>
    )
  }
}

const css = {
  bodyContainer: {
    margin: "20px 0 10px 0",
  },

}

export default styles(css, PostBody)
