import React from 'react'
import Post from './Post'
import { styles } from 'refire-app'
import { Spinner } from 'elemental'

const Posts = ({
  posts,
  user,
  locked,
  isAdmin,
  deletePost,
  updateQuote,
  toggleUpvote,
  selectLastPage,
  filterCategory,
  filterHashtag,
  styles,
  theme,
  thread,
}) => {
  if (!posts.length) {
    return (
      <div className={styles.spinnerContainer}>
        <Spinner />
      </div>
    )
  } else {
    if(filterCategory==="Instruction") {
      posts = posts.filter(function(post){
        if(post.value.category!==undefined) {
          return post.value.category === "3";
        }
      });
    } else if(filterCategory==="Ingredients") {
      posts = posts.filter(function(post){
        if(post.value.category!==undefined) {
          return post.value.category === "2";
        }
      });
    } else if(filterCategory==="Knowledge") {
      posts = posts.filter(function(post){
        if(post.value.category!==undefined) {
          return post.value.category === "4";
        }
      });
    }

    if(filterHashtag!=="Clear") {
      posts = posts.filter(function(post){
        console.log(post)
        if(post.value.hashtags!==undefined) {
          return post.value.hashtags.includes(filterHashtag);
        }
      });
    }
    return (
      <div>
        {
          posts.map(({ key, value: post }) => {
            return (
              <Post
                key={key}
                postKey={key}
                post={post}
                user={user}
                locked={locked}
                isAdmin={isAdmin}
                deletePost={deletePost}
                updateQuote={updateQuote}
                toggleUpvote={toggleUpvote}
                selectLastPage={selectLastPage}
                styles={theme.Post}
                theme={theme}
                thread={thread}
              />
            )
          })
        }
      </div>
    )
  }
}

const css = {
  spinnerContainer: {
    padding: "30px 0",
  },
}

export default styles(css, Posts)
