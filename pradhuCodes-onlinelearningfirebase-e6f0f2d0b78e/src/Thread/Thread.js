import React, { Component } from 'react'
import { styles } from 'refire-app'
import { Card } from 'elemental'
import LockIcon from 'react-icons/lib/fa/lock'
import Chip from 'material-ui/Chip';
import ReplyToThread from './ReplyToThread'
import Posts from './Posts'
import ShowPagination from './ShowPagination'
import DeleteDialog from './DeleteDialog'
import LockDialog from './LockDialog'
import DeletePostDialog from './DeletePostDialog'
import TopToolbar from './TopToolbar'
import DeleteButton from './DeleteButton'
import LockButton from './LockButton'
import Recipe from './Recipe'
import { Button, ButtonGroup, Pill, Form, FormSelect } from 'elemental'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';

class Thread extends Component {

constructor(props, context) {
    super(props, context)
    this.state = {
      filterCategory: "General",
      filterHashtag: "Clear", 
    }
  }

  handleSelect = (input) => {
    this.setState({ filterCategory: input });
  }

  handleHashtagSelect(hashtag) {
    this.setState({
      filterHashtag : hashtag
    });
  }

  render() {
    const {
      threadKey,
      thread,
      posts,
      settings,
      pagedPosts,
      user,
      isAdmin,
      styles,
      theme,
      state: {
        currentPage,
        deleteDialogVisible,
        deletePostDialogVisible,
        lockDialogVisible,
        postKey,
        quote,
      },
      stateSetters: {
        deletePost,
        deleteThread,
        handlePageSelect,
        hideDeleteDialog,
        hideDeletePostDialog,
        hideLockDialog,
        selectLastPage,
        showDeleteDialog,
        showDeletePostDialog,
        showLockDialog,
        toggleLocked,
        updateQuote,
        toggleUpvote,
        saveEditedPost,
      },
    } = this.props
    const { THREAD_PAGE_SIZE, THREAD_PAGE_LIMIT } = settings
    const style = {
      margin: 12,
    };
    const locked = thread.locked
      ? <LockIcon size="22px" />
      : <span />
    var hashtagData = "";
    posts.forEach(function(post) {
      if(post.value.hashtags !== undefined)
        hashtagData += post.value.hashtags;
    });
    hashtagData = hashtagData.split("#");
    let hashtagMap = new Map()
    hashtagData.forEach((hashtag)=>{
      hashtagMap.set(hashtag,hashtag);
    });
    hashtagData = Array.from(hashtagMap.values());
    return (
      <MuiThemeProvider>
      <div>
        <DeleteDialog
          visible={deleteDialogVisible}
          hide={hideDeleteDialog}
          save={deleteThread}
          title={thread.title}
          styles={theme.DeleteDialog}
        />
        <LockDialog
          visible={lockDialogVisible}
          hide={hideLockDialog}
          save={toggleLocked}
          title={thread.title}
          locked={thread.locked}
          styles={theme.LockDialog}
        />
        <DeletePostDialog
          visible={deletePostDialogVisible}
          hide={hideDeletePostDialog}
          save={deletePost}
          styles={theme.DeletePostDialog}
        />
       
        <Card className={styles.container}>
          <div className={styles.paginationContainer}>
            <div className={styles.headerContainer}>
              <div className={styles.lockContainer}>
                {locked}
              </div>
              <h2 className={styles.header}>
                {thread.title}
              </h2>
            </div>
             
            <TopToolbar
              isAdmin={isAdmin}
              posts={posts}
              pageSize={THREAD_PAGE_SIZE}>
              <ShowPagination
                currentPage={currentPage}
                handlePageSelect={handlePageSelect}
                posts={posts}
                pageSize={THREAD_PAGE_SIZE}
                pageLimit={THREAD_PAGE_LIMIT}
              />
              <div className={styles.buttonsContainer}>
                <DeleteButton
                  visible={isAdmin}
                  confirmDelete={showDeleteDialog}
                />
                <LockButton
                  visible={isAdmin}
                  locked={thread.locked}
                  confirmLockedChange={showLockDialog}
                />
              </div>
            </TopToolbar>
          </div>
          <div>
          <Recipe
            Ingredient={thread.Ingredient}
            posts={pagedPosts}
          />
          </div>
          <br/>
          <div>
            <h3>Comments</h3>
            <Form>
              <FormSelect options={[
              { label: 'Ingredients',  value: 'Ingredients' },
              { label: 'Instruction', value: 'Instruction'},
              { label: 'Knowledge', value: 'Knowledge'},
              ]} firstOption="General" onChange={this.handleSelect} />
            </Form>
          </div>
          <div>
          {hashtagData.map(function(hashtag) {
              if(hashtag!=="")
                return (<RaisedButton  onClick={() => this.handleHashtagSelect(hashtag)} label={"#"+hashtag} labelStyle={{textTransform: "none"}} primary={true} style={{margin:"5px",radius: "10px"}}/>)
          },this)}
          <RaisedButton label="Clear" onClick={() => this.handleHashtagSelect("Clear")} labelStyle={{textTransform: "none"}} style={style} />
          </div>
          <Posts
            posts={pagedPosts}
            Ingredient={thread.Ingredient}
            deletePost={showDeletePostDialog}
            updateQuote={updateQuote}
            toggleUpvote={toggleUpvote}
            saveEditedPost={saveEditedPost}
            user={user}
            filterCategory={this.state.filterCategory}
            filterHashtag={this.state.filterHashtag}
            locked={thread.locked}
            isAdmin={isAdmin}
            theme={theme}
            thread={thread}
          />
          <div className={styles.paginationContainer}>
            <ShowPagination
              currentPage={currentPage}
              handlePageSelect={handlePageSelect}
              posts={posts}
              pageSize={THREAD_PAGE_SIZE}
              pageLimit={THREAD_PAGE_LIMIT}
            />
          </div>
        </Card>

        <ReplyToThread
          user={user}
          IngredientData={thread.Ingredient}
          threadKey={threadKey}
          postKey={postKey}
          quote={quote}
          locked={thread.locked}
          selectLastPage={selectLastPage}
          styles={theme.ReplyToThread}
          theme={theme}
        />
      </div>
      </MuiThemeProvider>
    )
  }
}

const css = {
  container: {},
  header: {
    minHeight: "28px",
    margin: "0em 0 1em 0",
    display: "inline-block",
  },
  lockContainer: {
    display: "inline-block",
    verticalAlign: "top",
    paddingTop: "4px",
    paddingRight: "5px",
  },
  paginationContainer: {
    position: "relative",
    minHeight: "32px",
  },
  buttonsContainer: {
    display: "inline-block",
  },
  chip: {
    margin: 4,
  },
  wrapper: {
    display: 'flex',
    flexWrap: 'wrap',
  },
}

export default styles(css, Thread)
